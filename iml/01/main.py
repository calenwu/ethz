import numpy as np
import pandas as pd
from sklearn import linear_model

df = pd.read_csv('train.csv')
df = df[['x1','x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'y']]
features = df[['x1','x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10']].to_numpy() #Converts the dataframe to numpy array
target = df['y'].to_numpy()
clf = linear_model.LinearRegression()
clf.fit([[x for x in xn] for xn in features],
		[y for y in target])
coefs = np.array(clf.coef_)


df = pd.read_csv('./test.csv')
df = df[['Id','x1','x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10']]
ids = df['Id']
features = df[['x1','x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10']].to_numpy()


f = open('./submit.csv', 'w')
f.write('Id,y\n')
for x in range(len(ids)):
	f.write(str(ids[x]) + ',' + str(coefs.dot(features[x])) + '\n')
f.close()